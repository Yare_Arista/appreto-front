/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('retoLanding')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
